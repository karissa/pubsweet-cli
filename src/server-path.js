const path = require('path')

module.exports = () => path.resolve(
  process.cwd(), '.', 'node_modules', 'pubsweet-server'
)
