import React from 'react'
import { Route } from 'react-router'

import { requireAuthentication } from 'pubsweet-client/src/components/AuthenticatedComponent'

// Manage
import Manage from 'pubsweet-component-manage/Manage'
import PostsManager from 'pubsweet-component-posts-manager/PostsManager'

// Public
import Blog from 'pubsweet-component-blog/Blog'

// Authentication
import Login from 'pubsweet-component-login/Login'
import Signup from 'pubsweet-component-signup/Signup'

const AuthenticatedManage = requireAuthentication(
  Manage, 'create', (state) => state.collections[0]
)

export default (
  <Route>
    <Route path='/' component={Blog} />

    <Route path='/manage' component={AuthenticatedManage}>
      <Route path='posts' component={PostsManager} />
    </Route>

    <Route path='/login' component={Login} />
    <Route path='/signup' component={Signup} />
  </Route>
)
